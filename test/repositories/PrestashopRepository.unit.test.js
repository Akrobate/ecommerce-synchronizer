'use strict'

const {
    PrestashopRepository
} = require('../../src/repositories/PrestashopRepository');

const product_seed = {
    title: 'Lot de 13 J\'aime Lire de 6 ans à 10 ans',
    description: "Réductions sur un lot \n\nEn bon état, certains bord un peu usé voir photos et tout les livres n'ont pas la premier page cartonné avec les cartes voir photos.\n\nVoir les autres livres enfant: #emma9400livreenfant\n\n\n#jaimelire\n#jaimelirecp\n#livrejaimelire \n#jaimelire6ans\n#premierelecture\n#lecturejeunesse \n#apprentissagelecture \n#lectureseul \n#lectureenfant\n#livre7ans\n#livre6ans\n#livre10ans\n#livreenfant \n#emma9400livreenfant\n#emma9400livrejeunesse\n#éducatif \n#albumjeunesse \n#livreeducatif  \n#livreludique \n#livrejeunesseemma9400 \n#jeunesselivre \n#éditionjeunesse \n#livrecpniveau1\n#livrebouquinaixjeunesse",
    price_numeric: '12.10',
    category_list: [2],
    active: true,
}

describe('PrestashopRepository unit', () => {

    const prestashop_repository = new PrestashopRepository();

    it('Test', (done) => {
        const converted_xml = prestashop_repository
            .productConvertToXml(product_seed);

        console.log(converted_xml);
        done();
    })

});

