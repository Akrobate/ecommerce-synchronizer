module.exports = {
    configuration: {
        vinted_user_id: 10000000,
        cookie_content_file_path: './cookie_content.txt',
        prestashop: {
            host: 'PRESTASHOP_API_HOST',
            api_key: 'PRESTASHOP_API_KEY',
        },
        storage: {
            files: {
                application_files: './data/',
                images: './data/images/',
            },
        },
    },
};
