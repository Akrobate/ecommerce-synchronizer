# ecommerce-synchronizer

Feed an Prestashop Ecommerce from an existing Vinted account

Steps:

1. Download public vinted data
2. Download public images files data
3. Remap categories
4. Insert data Prestashop API

## Download public vinted data

* access to vinted from your browser
* inspect element network and copy cookie text data and put in in cookie_content.txt
* Search for any XHR call, and extract the cookie from this place
* Configure configuration.js file:

```javascript
module.exports = {
    configuration: {
        vinted_user_id: YOUR_VINTED_USED_ID,
        cookie_content_file_path: './cookie_content.txt',
        [...]
    },
};
```
* launch vintedProductsDownload.js script

The downloaded data is saved in Json format in path defined by configuration

```javascript
module.exports = {
    [...]
    storage: {
        files: {
            application_files: './data/',
        },
    },
};
```

### Information about data

Downloaded data is stored in /.application/json/vinted_products.json 
it is a simple json file with all downloaded data stored as plain objects. No mappings are done on this step. Data is stored as it.

Downloaded data is saved in data


## Download public images files data

* Requires public data be downloaded
* launch imageFilesDownload.js

```sh
node imageFilesDownload.js
```

All downloaded files will be stored in the path configured in configuration.js file

```javascript
module.exports = {
    [...]
    storage: {
        files: {
            images: './data/images/',
        },
    },
};
```


## Categories

### Categories Mappings

To import products in right categories you'll need to list your prestashop categories, and add inside the vinted category id.

You can find the mapping in data folder categories_mapper.json
```json
[
    {
        "name": "Pour les grands",
        "fullname": "Enfants / Livres / Les grands",
        "vinted_id_list": [2363],
        "prestashop_id": 29
    },
    {
        "name": "Pour les petits",
        "fullname": "Enfants / Livres / Les petits",
        "vinted_id_list": [2364],
        "prestashop_id": 30
    },
    {
        "name": "Pour les tout petits",
        "fullname": "Enfants / Livres / Les tout petits",
        "vinted_id_list": [2365],
        "prestashop_id": 31
    }
]
```

Each prestashop category can have multiple vinted categories

Once the referential is ready run script 

```bash
node categorizeProducts.js
```

### Categories discovery

You can run the script extractCatalogId to discover all existing categories on your vinted dump

```bash
node extractCatalogId.js
```

The category extractCatalog will display the tree of all categories where you have some products, with a count on each category summing the subcategories totals. Its a recursive script.

example of the script return

```
 - Divertissement (2328): 264

     - Musique et vidéo (2328): 13

         - Musique (2328): 6
             - CD (2328): 6

         - Vidéo (2333): 7
             - DVD (2333): 4
             - VHS (2334): 3

     - Livres (2336): 247

         - Littérature et fiction (2336): 41
             - Littérature classique (2336): 16
             - Bandes dessinées (2337): 6
             - Fiction contemporaine (2338): 15
             - Policiers et thrillers (2339): 1
             - Humour (2340): 1
             - Poésie et théâtre (2341): 1
             - Autre (2344): 1

         - Non-fiction (2345): 66
             - Arts et divertissement (2345): 14
             - Biographies et mémoires (2346): 5
             - Informatique et technologie (2348): 1
             - Cuisine (2349): 1
             - Santé et bien-être (2350): 11
             - Histoire (2351): 6
             - Famille (2353): 3
             - Politique et société (2354): 14
             - Éducation et formation (2355): 3
             - Livres de référence (2356): 2
             - Religion et spiritualité (2357): 1
             - Science et nature (2358): 3
             - Sports (2359): 1
             - Autre (2362): 1

         - Enfants et jeunes adultes (2363): 140
             - Jeunes adultes (2363): 3
             - Enfants (2364): 136
             - Bébés (2365): 1
```

### Projects vinted cateogies referential

This project do not include the whole Vinted categories referential. Only some of them are included. You can check wich ones are included by running the function getUnknownCatalogReferential from the extractCatalodId.js

When a category is unknown it can be added to the file ./referentials/VintedCatalogReferential.js



## Todo:

* Make diff updates for public data fetch
* Make diff update for Prestashop stuff
* Make diff update on images storages
* Check removal of product removes images
* Test script for updateing products

