

const {
    ProductsJsonRepository,
} = require('./repositories');

const mapping = require('./referentials/VintedCatalogReferential');
const products_json_repository = ProductsJsonRepository.getInstance();


function getUnknownCatalogReferential(product_list, vinted_catalog_referential) {
    const catalog_id_list = product_list.map((item) => item.catalog_id);
    const result = {};
    catalog_id_list.forEach((catalog_id) => {
        if (result[catalog_id] === undefined) {
            result[catalog_id] = 1;
        } else {
            result[catalog_id]++;
        }
    });
    vinted_catalog_referential.forEach((element) => {
        delete result[element.id];
    });
    return result;
}


function treePrint(tree) {
    const printItem = (subtree, level = 0) => {
        Object.keys(subtree).forEach((item) => {
            if (item !== '__count' && item !== '__id') {

                let indentor = '';
                for (let i = 0; i < level; i++) {
                    indentor += '    ';
                }

                if (Object.keys(subtree[item]).length !== 2) {
                    console.log('');
                }
                console.log(`${indentor} - ${item} (${subtree[item].__id}): ${subtree[item].__count}`);
                printItem(subtree[item], level + 1);
            }
        });
    };
    printItem(tree);
}


(async () => {
    const data = await products_json_repository.search();

    const result = getUnknownCatalogReferential(data, mapping);
    console.log('=============================================================');
    console.log('List of unknown categories of products with its product count');
    console.log(result);

    const enriched_mapping = mapping.map((category) => {

        const products_count = data
            .map((item) => item.catalog_id)
            .filter((catalog_id) => catalog_id === category.id)
            .length;

        const name_path = category.full_name.split('/')
            .map((category_part) => category_part.trim());
        return {
            ...category,
            name_path,
            products_count,
        };
    });

    const tree = {};
    enriched_mapping.forEach((emap) => {

        let tmp_subtree = tree;
        emap.name_path.forEach((sub_cat) => {
            if (tmp_subtree[sub_cat] === undefined) {
                tmp_subtree[sub_cat] = {
                    __count: emap.products_count,
                    __id: emap.id,
                };
            } else {
                tmp_subtree[sub_cat].__count += emap.products_count;
            }
            tmp_subtree = tmp_subtree[sub_cat];
        });
    });


    console.log('');
    console.log('================ Products repartition per Vinted categories =====================');
    console.log('');

    treePrint(tree);

})();
