'use strict';

const {
    ProductsJsonRepository,
} = require('./repositories');

const products_json_repository = ProductsJsonRepository.getInstance();

(async () => {
    const data = await products_json_repository.search();
    console.log('Products: ', data.length);
})();
