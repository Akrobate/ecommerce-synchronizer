'use strict';

const {
    prestashop_repository,
} = require('./repositories/PrestashopRepository');


const PRODUCT_OFFSET = 19;

(async () => {
    for (let i = 0; i < 500; i++) {
        try {
            await prestashop_repository.readItem('products', PRODUCT_OFFSET + i);
            await prestashop_repository.deleteItem('products', PRODUCT_OFFSET + i);
        } catch (error) {
            console.log('err');
        }
    }
})();
