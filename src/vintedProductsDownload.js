/* eslint-disable no-process-exit */
const {
    exec,
} = require('child_process');
const utils = require('util');
const execute = utils.promisify(exec);
const {
    products_json_repository,
} = require('./repositories');
const {
    CommonHelper,
} = require('./helpers');
const {
    configuration,
} = require('../configuration');


const UPDATE_DOWNLOADED_PRODUCT_FILE = true;


async function fetchResults(page, per_page) {
    const result = await execute(`node src/fetch_worker ${page} ${per_page}`,
        {
            maxBuffer: 1024 * 5000,
        }
    );
    return JSON.parse(result.stdout);
}


(async () => {

    const per_page = 50;

    const {
        vinted_user_id,
    } = configuration;

    const initial_products_list = await fetchResults(1, per_page);

    if (initial_products_list.error) {
        console.log(initial_products_list.error);
        // eslint-disable-next-line no-process-exit
        process.exit();
    }

    const {
        pagination,
    } = initial_products_list;

    const {
        total_pages,
    } = pagination;

    let current_page = 1;
    console.log(pagination);

    while (current_page <= total_pages) {
        console.log('Processing : ', vinted_user_id, current_page, per_page);
        let items = [];
        try {
            const vinted_response = await fetchResults(current_page, per_page);
            if (vinted_response.items) {
                ({
                    items,
                } = vinted_response);
            } else {
                console.log('Parse problem', current_page, per_page);
            }
        } catch (error) {
            console.log('error', error.message);
        }

        console.log('items.length', items.length);
        current_page += 1;

        if (items.length === 0) {
            console.log('Terminated before end');
            process.exit();
        }

        if (UPDATE_DOWNLOADED_PRODUCT_FILE) {
            for (const item of items) {
                const result = await products_json_repository.readDownloadedProduct(item.id);
                if (result) {
                    await products_json_repository.updateDownloadedProduct(item.id, item);
                } else {
                    await products_json_repository.createDownloadedProduct(item);
                }
            }
        } else {
            for (const item of items) {
                const result = await products_json_repository.read(item.id);
                if (result) {
                    await products_json_repository.update(item.id, item);
                } else {
                    await products_json_repository.create(item);
                }
            }
        }

        await CommonHelper.wait();
    }
})();
