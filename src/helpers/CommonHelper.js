'use strict';

const fs = require('fs');
const axios = require('axios');

class CommonHelper {

    /**
     * @param {Number} duration
     * @returns {Void}
     */
    static wait(duration = 2000) {
        return new Promise((resolve) => {
            setTimeout(() => resolve(), duration);
        });
    }

    /**
     * @param {String} url
     * @param {String} file_path
     * @returns {Promise}
     */
    static downloadFile(url, file_path) {
        return axios({
            method: 'get',
            url,
            responseType: 'stream',
        })
            .then((response) => {
                response.data.pipe(fs.createWriteStream(file_path));
            });
    }
}

module.exports = {
    CommonHelper,
};
