'use strict';

const {
    prestashop_repository: _prestashop_repository,
} = require('../repositories/PrestashopRepository');

class PrestashopService {

    /**
     * @param {PrestashopRepository} prestashop_repository
     * @param {String} prestashop_api_key
     */
    constructor(
        prestashop_repository
    ) {
        this.prestashop_repository = prestashop_repository;
    }


    /**
     * @static
     */
    static get OUT_OF_STOCK() {
        return {
            ACCEPT_ORDERS: 1,
            DEFAULT_BEHAVIOUR_ORDERS: 2,
        };
    }


    /**
     * @param {Object} input
     * @returns {Promise}
     */
    async createProduct(input) {

        const {
            quantity,
            photo_list,
        } = input;

        const {
            product,
        } = await this.prestashop_repository
            .createItemJsonInput('products', this.productMapper(input));

        console.log(`Created product (id: ${product.id}) -  ${product.name}`);

        const [
            stock_availables,
        ] = product.associations.stock_availables;

        await this.updateProductStock({
            id: stock_availables.id,
            product_id: product.id,
            out_of_stock: PrestashopService.OUT_OF_STOCK.DEFAULT_BEHAVIOUR_ORDERS,
            quantity,
        });

        console.log(`Updated stock (id: ${product.id}) -  ${product.name}`);

        if (photo_list && photo_list.length > 0) {
            await this.uploadProductPhotoList(product.id, photo_list);
        }

        const {
            product: product_updated,
        } = await this.prestashop_repository
            .readItem('products', product.id);
        return product_updated;
    }


    /**
     * @param {String} product_id
     * @returns {Object}
     */
    async readProduct(product_id) {
        const {
            product,
        } = await this.prestashop_repository
            .readItem('products', product_id);
        return product;
    }

    // Need being tested @todo
    /**
     * @param {*} id
     * @param {*} input
     * @returns {Promise}
     */
    async updateProduct(id, input) {

        console.log('id', id);
        console.log('input', input);

        const {
            product,
        } = await this.prestashop_repository
            .patchItemJsonInput('products', id, this.productMapper(input));
        return product;
    }


    /**
     * @param {String} product_stock_id
     * @returns {Object}
     */
    async readProductStock(product_stock_id) {
        const {
            stock_available,
        } = await this.prestashop_repository
            .readItem('stock_availables', product_stock_id);
        return stock_available;
    }


    /**
     * @param {Object} input
     * @param {Number} input.id
     * @param {Number} input.product_id
     * @param {Number} input.out_of_stock
     * @param {Number} input.quantity
     * @returns {Object}
     */
    async updateProductStock(input) {
        const {
            id,
            id_product_attribute,
            product_id,
            out_of_stock,
            quantity,
            depends_on_stock,
            id_shop,
        } = input;

        const stock_availables_put = await this.prestashop_repository
            .putItemJsonInput('stock_availables', id, {
                stock_available: {
                    id,
                    id_product_attribute: id_product_attribute ? id_product_attribute : 0,
                    quantity,
                    depends_on_stock: depends_on_stock ? depends_on_stock : 0,
                    id_product: product_id,
                    out_of_stock,
                    id_shop: id_shop ? id_shop : 1,
                },
            });
        return stock_availables_put;
    }


    /**
     * @param {Object} product_id
     * @param {Object} input
     * @param {Number} input.id
     * @param {Number} input.product_id
     * @param {Number} input.out_of_stock
     * @param {Number} input.quantity
     * @returns {Object}
     */
    async updateProductStockByProductId(product_id, input) {
        const {
            id_product_attribute,
            product_id: input_product_id,
            out_of_stock,
            quantity,
            depends_on_stock,
            id_shop,
        } = input;

        const product = await this.readProduct(product_id);

        const stock_availables_put = await this.updateProductStock({
            id: product.associations.stock_availables[0].id,
            id_product_attribute,
            product_id: input_product_id,
            out_of_stock,
            quantity,
            depends_on_stock,
            id_shop,
        });

        return stock_availables_put;
    }


    /**
     * @param {*} product_id
     * @param {*} category_list
     * @returns {Object}
     */
    async updateProductsCategories(product_id, category_list) {
        const update_result = await this.prestashop_repository
            .patchItemJsonInput(
                'products',
                product_id,
                {
                    product: {
                        id: product_id,
                        associations: {
                            categories: {
                                category: category_list.map((category) => ({
                                    id: category,
                                })),
                            },
                        },
                    },
                }
            );
        return update_result;
    }


    /**
     * @param {Number} product_id
     * @param {Array<String>} image_path_list
     * @return {Promise}
     */
    async uploadProductPhotoList(product_id, image_path_list) {
        const results = [];
        for (let i = 0; i < image_path_list.length; i++) {
            console.log(`Product id: ${product_id} - Uploading image ${i + 1}`);
            const image_result = await this.prestashop_repository
                .postProductImage(product_id, image_path_list[i]);
            results.push(image_result);
        }
        return results;
    }


    /**
     * @param {Object} input
     * @return {Object}
     */
    async createCategory(input) {
        const {
            category,
        } = await this.prestashop_repository
            .createItemJsonInput('categories', this.categoryMapper(input));
        return category;
    }


    /**
     * @param {Object} category
     * @returns {Object}
     */
    categoryMapper(category) {
        const {
            active,
            name,
            id_parent,
            position,
            is_root_category,
            link_rewrite,
        } = category;

        const request = {
            category: {
                // id: id ? id : undefined,
                active: active ? 1 : 0,
                name: {
                    language: {
                        _attributes: {
                            id: 1,
                        },
                        _text: name,
                    },
                },
                link_rewrite: {
                    language: {
                        _attributes: {
                            id: 1,
                        },
                        _text: link_rewrite,
                    },
                },
                id_parent,
                position,
                is_root_category: is_root_category ? 1 : 0,
            },
        };
        return request;
    }


    /**
     * @param {Object} product
     * @returns {Object}
     */
    productMapper(product) {

        const {
            title,
            description,
            price,
            category_list,
            active,
        } = product;

        const request = {
            product: {
                active: active ? 1 : 0,
                name: {
                    language: {
                        _attributes: {
                            id: 1,
                        },
                        _text: title,
                    },
                },
                description: {
                    language: {
                        _attributes: {
                            id: 1,
                        },
                        _text: description,
                    },
                },
                price,
                state: 1,
                show_price: 1,
                minimal_quantity: 1,
                available_for_order: 1,
                low_stock_alert: 0,
                advanced_stock_management: 0,
                unity: 0,
                out_of_stock: 0,
                associations: {
                    categories: {
                        category: category_list.map((category) => ({
                            id: category,
                        })),
                    },
                },
            },
        };

        return request;
    }

}

module.exports = {
    PrestashopService,
    prestashop_service: new PrestashopService(
        _prestashop_repository
    ),
};
