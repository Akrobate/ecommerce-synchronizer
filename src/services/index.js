'use strict';

const {
    PrestashopService,
    prestashop_service,
} = require('./PrestashopService');

const {
    VintedToPrestashopService,
    vinted_to_prestashop_service,
} = require('./VintedToPrestashopService');

module.exports = {
    PrestashopService,
    prestashop_service,
    VintedToPrestashopService,
    vinted_to_prestashop_service,
};
