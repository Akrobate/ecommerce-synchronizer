'use strict';

const {
    products_json_repository: _products_json_repository,
} = require('../repositories');

const {
    prestashop_service: _prestashop_service,
} = require('./PrestashopService');

class VintedToPrestashopService {

    /**
     * @param {PrestashopService} prestashop_service
     * @param {ProductsJsonRepository} products_json_repository
     */
    constructor(
        prestashop_service,
        products_json_repository
    ) {
        this.prestashop_service = prestashop_service;
        this.products_json_repository = products_json_repository;
    }


    /**
     * @param {Object} vinted_product
     * @returns {Object}
     */
    async createVintedProductInPrestashop(vinted_product) {
        const mapped_images = this.mapVintedImagesList(vinted_product.photos);

        const result = await this.prestashop_service.createProduct({
            ...vinted_product,
            price: vinted_product.price.amount,
            description: vinted_product.description.replaceAll('\n', '<br />'),
            category_list: [2],
            active: true,
            quantity: 1,
            photo_list: mapped_images,
        });
        return result;
    }


    /**
     * @param {Array} photo_list
     * @returns {Array}
     */
    mapVintedImagesList(photo_list) {
        const images_path = './data/images/';
        return [
            ...photo_list.filter((item) => item.is_main),
            ...photo_list.filter((item) => !item.is_main),
        ].map((item) => `${images_path}${item.id}.jpeg`);
    }
}

module.exports = {
    VintedToPrestashopService,
    vinted_to_prestashop_service: new VintedToPrestashopService(
        _prestashop_service,
        _products_json_repository
    ),
};
