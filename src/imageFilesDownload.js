'use strict';

const fs = require('fs');
const {
    CommonHelper,
} = require('./helpers');
const {
    products_json_repository,
} = require('./repositories');
const {
    configuration,
} = require('../configuration');

const path = require('path');

// Configuration
const DELAY = 1000;

/**
 * @param {Object} product
 * @returns {void}
 */
async function downloadAllProductPhotos(product) {
    for (const photo of product.photos) {
        const {
            full_size_url,
            id,
        } = photo;

        if (fs.existsSync(path.join(configuration.storage.files.images, `${id}.jpeg`))) {
            console.log(id, full_size_url, 'EXISTS');
        } else {
            console.log(id, full_size_url, 'DOWNLOADING...');
            await CommonHelper.wait(DELAY);
            try {
                await CommonHelper.downloadFile(
                    full_size_url,
                    path.join(configuration.storage.files.images, `${id}.jpeg`)
                );
            } catch (error) {
                console.log('Error', error.message);
            }
        }
    }
}


(async () => {
    const product_list = await products_json_repository.search();
    for (const product of product_list) {
        await downloadAllProductPhotos(product);
    }
})();
