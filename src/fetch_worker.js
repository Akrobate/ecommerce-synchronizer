'use strict';

const fs = require('fs');

const {
    vinted_repository,
} = require('./repositories');

const {
    configuration,
} = require('../configuration');

const [
    ,
    ,
    current_page,
    per_page,
] = process.argv;

(async () => {

    const {
        vinted_user_id,
        cookie_content_file_path,
    } = configuration;

    const cookie_content = fs.readFileSync(
        cookie_content_file_path,
        {
            encoding: 'utf8',
        }
    );
    try {
        const initial_products_list = await vinted_repository
            .fetchVinted(cookie_content, vinted_user_id, current_page, per_page);
        console.log(JSON.stringify(initial_products_list));
    } catch (error) {
        console.log(JSON.stringify({
            error: error.message,
        }));
    }
})();
