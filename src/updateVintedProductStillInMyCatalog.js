/**
 * This script calculates the differences between the last download
 * and the current products list
 *
 * if some products are not available anymore in fresh download, they will
 * get property setted:
 *  // DECIDE @todo
 *
 */


const {
    products_json_repository,
} = require('./repositories');

// Script configuration:
const MERGE_DOWNLOADED_PRODUCTS_TO_MAIN_PRODUCTS_LIST = false;


(async () => {

    const product_list = await products_json_repository.search();
    const last_download_product_list = await products_json_repository.searchDownloadedProduct();

    const new_products = last_download_product_list
        .filter(
            (product) => undefined === product_list.find((_product) => product.id === _product.id));

    const not_existing_products = product_list
        .filter(
            (product) => undefined === last_download_product_list.find(
                (_product) => product.id === _product.id
            )
        );

    console.log('total products', product_list.length);
    console.log('total last downloaded', last_download_product_list.length);
    console.log('new products', new_products.length);
    console.log('not existing products', not_existing_products.length);

    for (const product of product_list) {
        await products_json_repository.upsertProductStatus({
            id: product.id,
            status: 'archive',
        });
    }

    for (const product of last_download_product_list) {
        const product_status = await products_json_repository.readProductStatus(product.id);
        if (product_status === undefined) {
            await products_json_repository.createProductStatus(
                {
                    id: product.id,
                    status: 'new',
                }
            );
        } else {
            await products_json_repository.updateProductStatus(
                product.id,
                {
                    id: product.id,
                    status: 'still_exists',
                }
            );
        }
    }

    const product_status_list = await products_json_repository.searchProductStatus();
    // console.log(product_status_list);
    console.log('========= FROM STATUSES ================');
    console.log('total products', product_status_list.length);
    console.log('total last downloaded', product_status_list.filter((item) => ['new', 'still_exists'].includes(item.status)).length);
    console.log('new products', product_status_list.filter((item) => ['new'].includes(item.status)).length);
    console.log('not existing products', product_status_list.filter((item) => ['archive'].includes(item.status)).length);


    // Merge downloaded products to main products
    if (MERGE_DOWNLOADED_PRODUCTS_TO_MAIN_PRODUCTS_LIST) {
        for (const product of last_download_product_list) {
            const product_to_update = await products_json_repository.read(product.id);
            if (product_to_update === undefined) {
                await products_json_repository.create(product);
            } else {
                await products_json_repository.update(
                    product_to_update.id,
                    product
                );
            }
        }
    }
})();
