'use strict';

const {
    JsonFileRepository,
    products_json_repository,
    product_id_association_repository,
} = require('./repositories');
const {
    prestashop_service,
} = require('./services');

const json_file_repository = JsonFileRepository.getInstance();

(async () => {
    const categories_mapper = await json_file_repository.search('categories_mapper.json');
    const vinted_data_list = await products_json_repository.search();
    const association_referentail = await product_id_association_repository.search();

    for (const vinted_data of vinted_data_list) {
        const match = categories_mapper
            .find((item) => item.vinted_id_list.includes(vinted_data.catalog_id));
        if (match) {
            const {
                prestashop_id,
            } = association_referentail
                .find((item) => vinted_data.id === item.vinted_id);

            console.log(prestashop_id, match.prestashop_id);
            await prestashop_service.updateProductsCategories(prestashop_id, [match.prestashop_id]);
        }
    }
})();
