'use strict';

const fs = require('fs');
const {
    products_json_repository,
    vinted_repository,
} = require('./repositories');
const {
    configuration,
} = require('../configuration');
const {
    CommonHelper,
} = require('./helpers');

(async () => {

    const {
        vinted_user_id,
        cookie_content_file_path,
    } = configuration;

    const cookie_content = fs.readFileSync(
        cookie_content_file_path,
        {
            encoding: 'utf8',
        }
    );

    const per_page = 20;

    let initial_products_list = await vinted_repository
        .fetchVinted(cookie_content, vinted_user_id, 1, per_page);
    console.log(initial_products_list);
    console.log(cookie_content);

    await CommonHelper.wait(5000);

    initial_products_list = await vinted_repository
        .fetchVinted(fs.readFileSync(
            cookie_content_file_path,
            {
                encoding: 'utf8',
            }
        ), vinted_user_id, 1, per_page);

    console.log(initial_products_list);
    await CommonHelper.wait(5000);
    const {
        pagination,
    } = initial_products_list;

    const {
        total_pages,
    } = pagination

    let current_page = 1;
/*
    while (current_page <= total_pages) {
        console.log("=======", vinted_user_id, current_page, per_page)
        let items = [];
        try {
            const vinted_response = await vinted_repository
                .fetchVinted(cookie_content, vinted_user_id, current_page, per_page);

            items = vinted_response.items;
        } catch (error) {
            console.log("error", error.message)
        };
        current_page += 1;

        console.log("items.length", items.length)

        for (let i = 0; i < items.length; i++) {
            const result = await products_json_repository.read(items[i].id);
            if (!result) {
                await products_json_repository.create(items[i]);
            }
        }
        await wait();
    }

    // const  = await products_json_repository.search();
    console.log(test);

*/

    /*
    const results = await vinted_repository
        .fetchVinted(cookie_content, vinted_user_id, 2, 5);

    const mapped_results = results.items.map((item) => ({
        id: item.id,
        title: item.title,
        description: item.description,
        price_numeric: item.price_numeric,
        original_price_numeric: item.original_price_numeric,
        currency: item.currency,
        isbn: item.isbn,
        created_at_ts: item.created_at_ts,
        updated_at_ts: item.updated_at_ts,
        user_updated_at_ts: item.user_updated_at_ts,
    }))

    console.log(mapped_results);
    console.log(results.pagination)
    */
})();
