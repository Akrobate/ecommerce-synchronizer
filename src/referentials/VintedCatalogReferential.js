'use strict';

const mapping = [
    {
        id: 20,
        full_name: 'Femmes / Accessoires / Ceintures',
        name: 'Ceintures',
    },
    {
        id: 158,
        full_name: 'Femmes / Sacs / Sacs à bandoulière',
        name: 'Sacs à bandoulière',
    },
    {
        id: 159,
        full_name: 'Femmes / Sacs / Pochettes',
        name: 'Pochettes',
    },
    {
        id: 162,
        full_name: 'Femmes / Accessoires / Bijoux / Autres bijoux',
        name: 'Autres bijoux',
    },
    {
        id: 164,
        full_name: 'Femmes / Accessoires / Bijoux / Colliers',
        name: 'Colliers',
    },
    {
        id: 167,
        full_name: 'Femmes / Accessoires / Bijoux / Broches',
        name: 'Broches',
    },
    {
        id: 221,
        full_name: 'Femmes / Vêtements / Hauts et t-shirts / T-shirts',
        name: 'T-shirts',
    },
    {
        id: 232,
        full_name: 'Femmes / Accessoires / Chapeaux & casquettes / Bonnets',
        name: 'Bonnets',
    },
    {
        id: 244,
        full_name: 'Hommes / Accessoires / Bijoux / Autre',
        name: 'Autre',
    },
    {
        id: 1140,
        full_name: 'Femmes / Accessoires / Autres accessoires',
        name: 'Autres accessoires',
    },
    {
        id: 1258,
        full_name: 'Enfants / Filles / Sacs et sacs à dos',
        name: 'Sacs et sacs à dos',
    },
    {
        id: 1493,
        full_name: 'Femmes / Sacs / Sacs ethniques et traditionnels',
        name: 'Sacs ethniques et traditionnels',
    },
    {
        id: 1502,
        full_name: 'Enfants / Autres',
        name: 'Autres',
    },
    {
        id: 1511,
        full_name: 'Enfants / Poussettes / Accessoires poussettes',
        name: 'Accessoires poussettes',
    },
    {
        id: 1525,
        full_name: 'Enfants / Filles / Chaussures / Chaussures bébé',
        name: 'Chaussures bébé',
    },
    {
        id: 1535,
        full_name: 'Enfants / Filles / Chemises et t-shirts / T-shirts',
        name: 'T-shirts',
    },
    {
        id: 1551,
        full_name: 'Enfants / Filles / Pulls & sweats / Gilets',
        name: 'Gilets',
    },
    {
        id: 1553,
        full_name: 'Enfants / Filles / Robes / Robes longues',
        name: 'Robes longues',
    },
    {
        id: 1642,
        full_name: 'Enfants / Garçons / Bébé garçons / Combinaisons',
        name: 'Combinaisons',
    },
    {
        id: 1730,
        full_name: 'Enfants / Jouets / Figurines',
        name: 'Figurines',
    },
    {
        id: 1731,
        full_name: 'Enfants / Jouets / Poupées',
        name: 'Poupées',
    },
    {
        id: 1763,
        full_name: 'Enfants / Jouets / Jouets éducatifs',
        name: 'Jouets éducatifs',
    },
    {
        id: 1764,
        full_name: 'Enfants / Jouets / Animaux en peluche',
        name: 'Animaux en peluche',
    },
    {
        id: 1766,
        full_name: 'Enfants / Jouets / Jouets musicaux',
        name: 'Jouets musicaux',
    },
    {
        id: 1767,
        full_name: 'Enfants / Jouets / Jeux de construction',
        name: 'Jeux de construction',
    },
    {
        id: 1768,
        full_name: 'Enfants / Jouets / Jouets en bois',
        name: 'Jouets en bois',
    },
    {
        id: 1770,
        full_name: 'Enfants / Jouets / Jouets d\'eau',
        name: 'Jouets d\'eau',
    },
    {
        id: 1875,
        full_name: 'Enfants / Filles / Bébé filles / Autre',
        name: 'Autre',
    },
    {
        id: 1877,
        full_name: 'Enfants / Filles / Pulls & sweats / Autre',
        name: 'Autre',
    },
    {
        id: 1881,
        full_name: 'Enfants / Filles / Accessoires / Autres accessoires',
        name: 'Autres accessoires',
    },
    {
        id: 1937,
        full_name: 'Maison / Décoration / Encadrements',
        name: 'Encadrements',
    },
    {
        id: 1963,
        full_name: 'Maison / Décoration / Rangements / Boîtes',
        name: 'Boîtes',
    },
    {
        id: 1968,
        full_name: 'Maison / Décoration / Miroirs / Miroirs muraux',
        name: 'Miroirs muraux',
    },
    {
        id: 2008,
        full_name: 'Maison / Arts de la table / Verres / Carafes',
        name: 'Carafes',
    },
    {
        id: 2011,
        full_name: 'Maison / Arts de la table / Verres / Sets de verres',
        name: 'Sets de verres',
    },
    {
        id: 2086,
        full_name: 'Enfants / Jouets / Doudous',
        name: 'Doudous',
    },
    {
        id: 2328,
        full_name: 'Divertissement / Musique et vidéo / Musique / CD',
        name: 'CD',
    },
    {
        id: 2333,
        full_name: 'Divertissement / Musique et vidéo / Vidéo / DVD',
        name: 'DVD',
    },
    {
        id: 2334,
        full_name: 'Divertissement / Musique et vidéo / Vidéo / VHS',
        name: 'VHS',
    },
    {
        id: 2336,
        full_name: 'Divertissement / Livres / Littérature et fiction / Littérature classique',
        name: 'Littérature classique',
    },
    {
        id: 2337,
        full_name: 'Divertissement / Livres / Littérature et fiction / Bandes dessinées',
        name: 'Bandes dessinées',
    },
    {
        id: 2338,
        full_name: 'Divertissement / Livres / Littérature et fiction / Fiction contemporaine',
        name: 'Fiction contemporaine',
    },
    {
        id: 2339,
        full_name: 'Divertissement / Livres / Littérature et fiction / Policiers et thrillers',
        name: 'Policiers et thrillers',
    },
    {
        id: 2340,
        full_name: 'Divertissement / Livres / Littérature et fiction / Humour',
        name: 'Humour',
    },
    {
        id: 2341,
        full_name: 'Divertissement / Livres / Littérature et fiction / Poésie et théâtre',
        name: 'Poésie et théâtre',
    },
    {
        id: 2344,
        full_name: 'Divertissement / Livres / Littérature et fiction / Autre',
        name: 'Autre',
    },
    {
        id: 2345,
        full_name: 'Divertissement / Livres / Non-fiction / Arts et divertissement',
        name: 'Arts et divertissement',
    },
    {
        id: 2346,
        full_name: 'Divertissement / Livres / Non-fiction / Biographies et mémoires',
        name: 'Biographies et mémoires',
    },
    {
        id: 2348,
        full_name: 'Divertissement / Livres / Non-fiction / Informatique et technologie',
        name: 'Informatique et technologie',
    },
    {
        id: 2349,
        full_name: 'Divertissement / Livres / Non-fiction / Cuisine',
        name: 'Cuisine',
    },
    {
        id: 2350,
        full_name: 'Divertissement / Livres / Non-fiction / Santé et bien-être',
        name: 'Santé et bien-être',
    },
    {
        id: 2351,
        full_name: 'Divertissement / Livres / Non-fiction / Histoire',
        name: 'Histoire',
    },
    {
        id: 2353,
        full_name: 'Divertissement / Livres / Non-fiction / Famille',
        name: 'Famille',
    },
    {
        id: 2354,
        full_name: 'Divertissement / Livres / Non-fiction / Politique et société',
        name: 'Politique et société',
    },
    {
        id: 2355,
        full_name: 'Divertissement / Livres / Non-fiction / Éducation et formation',
        name: 'Éducation et formation',
    },
    {
        id: 2356,
        full_name: 'Divertissement / Livres / Non-fiction / Livres de référence',
        name: 'Livres de référence',
    },
    {
        id: 2357,
        full_name: 'Divertissement / Livres / Non-fiction / Religion et spiritualité',
        name: 'Religion et spiritualité',
    },
    {
        id: 2358,
        full_name: 'Divertissement / Livres / Non-fiction / Science et nature',
        name: 'Science et nature',
    },
    {
        id: 2359,
        full_name: 'Divertissement / Livres / Non-fiction / Sports',
        name: 'Sports',
    },
    {
        id: 2362,
        full_name: 'Divertissement / Livres / Non-fiction / Autre',
        name: 'Autre',
    },
    {
        id: 2363,
        full_name: 'Divertissement / Livres / Enfants et jeunes adultes / Jeunes adultes',
        name: 'Jeunes adultes',
    },
    {
        id: 2364,
        full_name: 'Divertissement / Livres / Enfants et jeunes adultes / Enfants',
        name: 'Enfants',
    },
    {
        id: 2365,
        full_name: 'Divertissement / Livres / Enfants et jeunes adultes / Bébés',
        name: 'Bébés',
    },
    {
        id: 2479,
        full_name: 'Divertissement / Jeux et puzzles / Casse-têtes',
        name: 'Casse-têtes',
    },
    {
        id: 2546,
        full_name: 'Enfants / Filles / Vêtements d\'extérieur / Vestes / Vestes en jean',
        name: 'Vestes en jean',
    },
];

module.exports = mapping;
