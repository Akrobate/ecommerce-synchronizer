'use strict';

const {
    prestashop_service,
} = require('./services');

const {
    products_json_repository,
    product_id_association_repository,
} = require('./repositories');

// const product_id = 367;
const product_id = 347;
(async () => {
    const product = await prestashop_service.readProduct(product_id);
    console.log(product);

    console.log(product.associations.stock_availables);

    const product_stock = await prestashop_service
        .readProductStock(product.associations.stock_availables[0].id);
    console.log(product_stock);

    await prestashop_service.updateProductStock({
        id: product_stock.id,
        product_id: product_stock.id_product,
        out_of_stock: 1,
        quantity: product_stock.quantity,
    });
})();
