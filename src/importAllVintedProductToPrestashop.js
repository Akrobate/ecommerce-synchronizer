'use strict';

const {
    vinted_to_prestashop_service,
} = require('./services/VintedToPrestashopService');

const {
    products_json_repository,
    product_id_association_repository,
} = require('./repositories');

(async () => {

    const vinted_data = await products_json_repository.search();
    const existing_vinted_id_list = (await product_id_association_repository.search())
        .map((id_association) => id_association.vinted_id);

    for (const vinted_item of vinted_data) {
        if (!existing_vinted_id_list.includes(vinted_item.id)) {
            const result = await vinted_to_prestashop_service
                .createVintedProductInPrestashop(vinted_item);
            await product_id_association_repository.create({
                vinted_id: vinted_item.id,
                prestashop_id: result.id,
            });
        }
    }

})();
