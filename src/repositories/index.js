const {
    JsonFileRepository,
} = require('./JsonFileRepository');
const {
    PrestashopRepository,
    prestashop_repository,
} = require('./PrestashopRepository');
const {
    ProductIdAssociationsRepository,
    product_id_association_repository,
} = require('./ProductIdAssociationsRepository');
const {
    ProductsJsonRepository,
    products_json_repository,
} = require('./ProductsJsonRepository');
const {
    VintedRepository,
    vinted_repository,
} = require('./VintedRepository');


module.exports = {
    JsonFileRepository,
    PrestashopRepository,
    ProductIdAssociationsRepository,
    ProductsJsonRepository,
    VintedRepository,
    prestashop_repository,
    product_id_association_repository,
    products_json_repository,
    vinted_repository,
};
