'use strict';

const {
    JsonFileRepository,
} = require('./JsonFileRepository');


class ProductsJsonRepository {

    /**
     * @static
     * @returns {ProductsJsonRepository}
     */
    static getInstance() {
        if (ProductsJsonRepository.instance === null) {
            ProductsJsonRepository.instance = new ProductsJsonRepository(
                new JsonFileRepository()
            );
        }

        return ProductsJsonRepository.instance;
    }


    /**
     * @param {*} json_file_repository
     */
    constructor(json_file_repository) {
        this.json_file_repository = json_file_repository;
        this.json_file_repository.setOption('formated_json_file', true);

        this.json_last_download_filename = 'vinted_last_download_products.json';
        this.repository_filename = 'vinted_products.json';
        this.products_status_filename = 'vinted_products_status.json';
    }


    /**
     * @param {Object} input
     * @returns {Array<Object>}
     */
    async search() {
        const results = await this.json_file_repository.search(this.repository_filename);
        return results;
    }


    /**
     * @param {Object} input
     * @returns {Array<Object>}
     */
    async create(input) {
        const results = await this.json_file_repository
            .create(this.repository_filename, input, false);
        return results;
    }


    /**
     * @param {string} id
     * @returns {Array<Object>}
     */
    async read(id) {
        const results = await this.json_file_repository.read(this.repository_filename, id);
        return results;
    }

    /**
     * @param {string} id
     * @param {Object} input
     * @returns {Array<Object>}
     */
    async update(id, input) {
        const results = await this.json_file_repository
            .update(this.repository_filename, id, input, true);
        return results;
    }


    /**
     * @param {Object} input
     * @returns {Array<Object>}
     */
    async searchDownloadedProduct() {
        const results = await this.json_file_repository
            .search(this.json_last_download_filename);
        return results;
    }

    /**
     * @param {Object} input
     * @returns {Array<Object>}
     */
    async createDownloadedProduct(input) {
        const results = await this.json_file_repository
            .create(this.json_last_download_filename, input, false);
        return results;
    }


    /**
     * @param {string} id
     * @returns {Array<Object>}
     */
    async readDownloadedProduct(id) {
        const results = await this.json_file_repository.read(this.json_last_download_filename, id);
        return results;
    }

    /**
     * @param {string} id
     * @param {Object} input
     * @returns {Array<Object>}
     */
    async updateDownloadedProduct(id, input) {
        const results = await this.json_file_repository
            .update(this.json_last_download_filename, id, input, true);
        return results;
    }


    /**
     * @param {Object} input
     * @returns {Array<Object>}
     */
    async searchProductStatus() {
        const results = await this.json_file_repository
            .search(this.products_status_filename);
        return results;
    }

    /**
     * @param {Object} input
     * @returns {Array<Object>}
     */
    async createProductStatus(input) {
        const results = await this.json_file_repository
            .create(this.products_status_filename, input, false);
        return results;
    }


    /**
     * @param {string} id
     * @returns {Array<Object>}
     */
    async readProductStatus(id) {
        const results = await this.json_file_repository.read(this.products_status_filename, id);
        return results;
    }

    /**
     * @param {string} id
     * @param {Object} input
     * @returns {Array<Object>}
     */
    async updateProductStatus(id, input) {
        const results = await this.json_file_repository
            .update(this.products_status_filename, id, input, true);
        return results;
    }

    /**
     * @param {Object} input
     * @returns {Array<Object>}
     */
    async upsertProductStatus(input) {
        const {
            id,
        } = input;

        if (id === undefined) {
            throw new Error('upsertProductStatus must have id as property');
        }

        const product_status = await this.readProductStatus(id);
        if (product_status === undefined) {
            return this.createProductStatus(input);
        }
        return this.updateProductStatus(
            id,
            input
        );
    }

}

ProductsJsonRepository.instance = null;

module.exports = {
    ProductsJsonRepository,
    products_json_repository: ProductsJsonRepository.getInstance(),
};
