'use strict';

const axios = require('axios');
const convert = require('xml-js');
const FormData = require('form-data');
const fs = require('fs');
const {
    configuration,
} = require('../../configuration');

class PrestashopRepository {

    /**
     * @param {String} prestashop_host
     * @param {String} prestashop_api_key
     */
    constructor(
        prestashop_host,
        prestashop_api_key
    ) {
        this.prestashop_host = prestashop_host;
        this.prestashop_api_key = prestashop_api_key;
    }


    /**
     * @param {Object} item
     * @returns {Schema}
     */
    async getSchema(item) {
        const schema = await axios.get(
            `${this.prestashop_host}/api/${item}?schema=synopsis`,
            this.getHeaders()
        );
        return schema.data;
    }


    /**
     * @returns {Object}
     */
    getHeaders() {
        return {
            headers: {
                Authorization: `Basic ${
                    Buffer.from(`${this.prestashop_api_key}:`).toString('base64')
                }`,
            },
        };
    }


    /**
     * @param {object} data
     * @returns {String}
     */
    genericJs2XmlConvert(data) {
        return convert.js2xml(
            {
                prestashop: data,
            },
            {
                compact: true,
                ignoreCdata: false,
                spaces: 2,
            }
        );
    }


    /**
     * @param {Object} item
     * @param {Number} id
     * @param {*} output_format
     * @returns {Object}
     */
    async readItem(item, id, output_format = 'JSON') {
        try {
            const response = await axios.get(
                `${this.prestashop_host}/api/${item}/${id}?output_format=${output_format}`,
                this.getHeaders()
            );
            return response.data;
        } catch (error) {
            console.log('readItem', item, error.response.data);
            throw new Error(error.response.data);
        }
    }


    /**
     * @param {*} item
     * @param {*} data
     * @param {*} output_format
     * @returns {Promise}
     */
    async createItemJsonInput(item, data, output_format = 'JSON') {
        const result = await this
            .createItem(item, this.genericJs2XmlConvert(data), output_format);
        return result;
    }


    /**
     * @param {Object} item
     * @param {String} xml_query_content
     * @param {String} output_format
     * @returns {Promise}
     */
    async createItem(item, xml_query_content, output_format = 'JSON') {
        try {
            const creation_response = await axios.post(
                `${this.prestashop_host}/api/${item}&output_format=${output_format}`,
                xml_query_content,
                this.getHeaders()
            );
            return creation_response.data;
        } catch (error) {
            console.log('createItem', item, error.response.data);
            throw new Error(error.response.data);
        }
    }


    /**
     * @param {*} item
     * @param {*} id
     * @param {*} data
     * @param {*} output_format
     * @returns {Promise}
     */
    async putItemJsonInput(item, id, data, output_format = 'JSON') {
        const result = await this
            .putItem(item, id, this.genericJs2XmlConvert(data), output_format);
        return result;
    }


    /**
     * @param {String} item
     * @param {Number} id
     * @param {string} xml_query_content
     * @param {String} output_format
     * @returns {object}
     */
    async putItem(item, id, xml_query_content, output_format = 'JSON') {
        try {
            const creation_response = await axios.put(
                `${this.prestashop_host}/api/${item}/${id}&output_format=${output_format}`,
                xml_query_content,
                this.getHeaders()
            );
            return creation_response.data;
        } catch (error) {
            console.log(error.response.data);
            console.log(`${this.prestashop_host}/api/${item}/${id}&output_format=${output_format}`);
            throw new Error('putItem error');
        }
    }


    /**
     * @param {*} item
     * @param {*} id
     * @param {*} data
     * @param {*} output_format
     * @returns {Promise}
     */
    async patchItemJsonInput(item, id, data, output_format = 'JSON') {
        console.log(this.genericJs2XmlConvert(data));
        const result = await this
            .patchItem(item, id, this.genericJs2XmlConvert(data), output_format);
        return result;
    }


    /**
     * @param {String} item
     * @param {Number} id
     * @param {string} xml_query_content
     * @param {String} output_format
     * @returns {object}
     */
    async patchItem(item, id, xml_query_content, output_format = 'JSON') {
        try {
            const creation_response = await axios.patch(
                `${this.prestashop_host}/api/${item}/${id}&output_format=${output_format}`,
                xml_query_content,
                this.getHeaders()
            );
            return creation_response.data;
        } catch (error) {
            console.log(error.response.data);
            console.log(`${this.prestashop_host}/api/${item}/${id}&output_format=${output_format}`);
            throw new Error('putItem error');
        }
    }


    /**
     * @param {String} item
     * @param {Number} id
     * @param {String} output_format
     * @returns {object}
     */
    async deleteItem(item, id, output_format = 'JSON') {
        try {
            const creation_response = await axios.delete(
                `${this.prestashop_host}/api/${item}/${id}`,
                this.getHeaders()
            );
            return creation_response.data;
        } catch (error) {
            console.log(error.response.data);
            console.log(error.response);
            console.log(`${this.prestashop_host}/api/${item}/${id}&output_format=${output_format}`);
            throw new Error('delete error');
        }
    }


    /**
     * @param {string} product_id
     * @param {string} image_file_path
     * @returns {object}
     */
    async postProductImage(product_id, image_file_path) {
        try {
            const form = new FormData();
            form.append('image', fs.createReadStream(image_file_path));
            const {
                data,
            } = await axios.post(
                `${this.prestashop_host}/api/images/products/${product_id}&output_format=JSON`,
                form,
                this.getHeaders()
            );
            return data;
        } catch (error) {
            console.log(error);
            throw new Error('postProductImage problem');
        }
    }

}

module.exports = {
    PrestashopRepository,
    prestashop_repository: new PrestashopRepository(
        configuration.prestashop.host,
        configuration.prestashop.api_key
    ),
};
