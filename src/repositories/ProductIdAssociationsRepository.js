'use strict';

const {
    JsonFileRepository,
} = require('./JsonFileRepository');


class ProductIdAssociationsRepository {

    /**
     * @static
     * @returns {ProductIdAssociationsRepository}
     */
    static getInstance() {
        if (ProductIdAssociationsRepository.instance === null) {
            ProductIdAssociationsRepository.instance = new ProductIdAssociationsRepository(
                new JsonFileRepository()
            );
        }

        return ProductIdAssociationsRepository.instance;
    }


    /**
     * @param {*} json_file_repository
     */
    constructor(json_file_repository) {
        this.json_file_repository = json_file_repository;
        this.json_file_repository.setOption('formated_json_file', true);

        this.repository_filename = 'vited_id_prestashop_id.json';
    }


    /**
     * @param {Object} input
     * @returns {Array<Object>}
     */
    async search() {
        const results = await this.json_file_repository.search(this.repository_filename);
        return results;
    }


    /**
     * @param {Object} input
     * @returns {Array<Object>}
     */
    async create(input) {
        const results = await this.json_file_repository
            .create(this.repository_filename, input, false);
        return results;
    }


    /**
     * @param {string} id
     * @returns {Array<Object>}
     */
    async read(id) {
        const results = await this.json_file_repository.read(this.repository_filename, id);
        return results;
    }

    /**
     * @param {string} id
     * @param {Object} input
     * @returns {Array<Object>}
     */
    async update(id, input) {
        const results = await this.json_file_repository
            .update(this.repository_filename, id, input, true);
        return results;
    }

}

ProductIdAssociationsRepository.instance = null;

module.exports = {
    ProductIdAssociationsRepository,
    product_id_association_repository: ProductIdAssociationsRepository.getInstance(),
};
