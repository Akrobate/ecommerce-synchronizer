'use strict';

const axios = require('axios');

class VintedRepository {

    /**
     * constructor
     */
    constructor() {
        this.host_url = 'https://www.vinted.fr';
    }


    /**
     * @param {*} cookie_content
     * @param {*} vinted_used_id
     * @param {*} page
     * @param {*} per_page
     * @param {*} order newest_first | relevance
     * @returns {Array}
     */
    async fetchVinted(cookie_content, vinted_used_id, page, per_page, order = 'newest_first') {

        // const axios_instance = axios.create();
        const axios_instance = axios.create();
        const {
            data,
        } = await axios_instance.get(
            `${this.host_url}/api/v2/users/${vinted_used_id}/items`,
            {
                headers: {
                    'Cookie': cookie_content,
                    'Cache-Control': 'no-cache',
                },
                params: {
                    page,
                    per_page,
                    order,
                },
            }
        );
        return data;
    }
}

module.exports = {
    VintedRepository,
    vinted_repository: new VintedRepository(),
};
