'use strict';

const fs = require('fs');
const path = require('path');
const {
    v4,
} = require('uuid');
const {
    configuration,
} = require('../../configuration');


class JsonFileRepository {

    /**
     * @static
     * @returns {JsonFileRepository}
     */
    static getInstance() {
        if (JsonFileRepository.instance === null) {
            JsonFileRepository.instance = new JsonFileRepository();
        }

        return JsonFileRepository.instance;
    }

    /**
     * @returns {JsonFileRepository}
     */
    constructor() {
        this.options = {
            formated_json_file: false,
        };
        this.DEFAULT_APPLICATION_FILE_PATH = '.application/json/';
    }


    /**
     * @param {string} key
     * @param {*} value
     * @returns {void}
     */
    setOption(key, value) {
        this.options[key] = value;
    }


    /**
     * @returns {void}
     */
    async checkAndInitApplicationFilePath() {
        const path_list = this.DEFAULT_APPLICATION_FILE_PATH.split('/');
        // eslint-disable-next-line guard-for-in
        for (const p in path_list) {
            const sub_path = path.join(
                configuration.storage.files.application_files, ...path_list.slice(0, p)
            );
            const exists = fs.existsSync(sub_path);
            if (!exists) {
                await fs.promises.mkdir(sub_path);
            }
        }
    }


    /**
     * @param {string} filename
     * @returns {void}
     */
    async initRepository(filename) {
        const file_path = path.join(
            configuration.storage.files.application_files,
            ...this.DEFAULT_APPLICATION_FILE_PATH.split('/'),
            filename
        );
        const exists = fs.existsSync(file_path);
        if (!exists) {
            await this.saveJsonToFile(filename, []);
        }
    }


    /**
     * @param {String} filename
     * @returns {void}
     */
    async init(filename) {
        await this.checkAndInitApplicationFilePath();
        await this.initRepository(filename);
    }


    /**
     * @param {*} filename
     * @param {*} input
     * @param {*} options
     * @returns {Promise<Array>}
     */
    async search(filename, input = {}, options = {}) {
        await this.init(filename);
        const {
            sort,
        } = options;

        let data = await this.getJsonFromFile(filename);

        const input_keys = Object.keys(input);
        data = data.filter((item) => {
            for (const key of input_keys) {
                if (item[key] !== input[key]) {
                    return false;
                }
            }
            return true;
        });

        if (sort) {
            const sort_props_list = Object.keys(sort);
            if (Object.keys(sort_props_list) > 1) {
                throw new Error('API Limitation: only on sort field is accepted');
            }
            const [
                sort_field,
            ] = sort_props_list;

            const sort_order = sort[sort_field];

            data.sort((a, b) => {
                if (a[sort_field] > b[sort_field]) {
                    return sort_order;
                } else if (a[sort_field] < b[sort_field]) {
                    return sort_order * -1;
                }
                return 1;
            });
        }

        return data;

    }


    /**
     * @param {String} filename
     * @param {Object} input
     * @param {Boolean} auto_id
     * @returns {Promise<Object>}
     */
    async create(filename, input, auto_id = true) {
        await this.init(filename);
        const data = await this.getJsonFromFile(filename);
        const data_item = input;
        if (auto_id) {
            data_item.id = v4();
        }
        data.push(data_item);
        await this.saveJsonToFile(filename, data);
        return data_item;
    }


    /**
     * @param {String} filename
     * @param {String} id
     * @returns {Promise<Object>}
     */
    async read(filename, id) {
        await this.init(filename);
        const data = await this.getJsonFromFile(filename);
        const element = data.find((item) => item.id === id);
        return element;
    }


    /**
     * @param {String} filename
     * @param {String} id
     * @param {Object} input
     * @param {Boolean} replace
     * @returns {Pormise<Object>}
     */
    async update(filename, id, input, replace = false) {
        await this.init(filename);
        const data = await this.getJsonFromFile(filename);
        const item_index = data.findIndex((item) => item.id === id);

        if (item_index < 0) {
            throw Error(`No item with id: ${id}`);
        }

        if (replace) {
            data[item_index] = input;
        } else {
            data[item_index] = {
                ...data[item_index],
                ...input,
            };
        }

        await this.saveJsonToFile(filename, data);
        return data[item_index];
    }


    /**
     * @param {string} filename
     * @param {string} id
     * @returns {Promise<Object>}
     */
    async delete(filename, id) {
        await this.init(filename);
        const data = await this.getJsonFromFile(filename);
        const item_index = data.findIndex((item) => item.id === id);
        if (item_index < 0) {
            throw Error(`No item with id: ${id}`);
        }
        data.splice(item_index, 1);
        await this.saveJsonToFile(filename, data);
        return {};
    }


    /**
     * @param {String} filename
     * @returns {Promise<Object>}
     */
    async getJsonFromFile(filename) {
        const data = await fs.promises.readFile(
            path.join(
                configuration.storage.files.application_files,
                ...this.DEFAULT_APPLICATION_FILE_PATH.split('/'),
                filename
            ),
            {
                encoding: 'utf8',
            }
        );
        return JSON.parse(data);
    }


    /**
     * @param {String} filename
     * @param {Promise<Object>} data
     * @returns {Promise<void>}
     */
    async saveJsonToFile(filename, data) {

        let string_json_data = '';
        if (this.options.formated_json_file) {
            string_json_data = JSON.stringify(data, null, 4);
        } else {
            string_json_data = JSON.stringify(data);
        }

        await fs.promises.writeFile(
            path.join(
                configuration.storage.files.application_files,
                ...this.DEFAULT_APPLICATION_FILE_PATH.split('/'),
                filename
            ),
            string_json_data,
            {
                encoding: 'utf8',
            }
        );
    }
}

JsonFileRepository.instance = null;

module.exports = {
    JsonFileRepository,
};
